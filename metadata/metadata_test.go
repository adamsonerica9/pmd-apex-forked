package metadata_test

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/pmd-apex/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "pmd-apex",
		Name:    "PMD.Apex",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "GitLab",
		},
		URL: "https://pmd.github.io",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
