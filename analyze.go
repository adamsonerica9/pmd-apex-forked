package main

import (
	"io"
	"os"
	"os/exec"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

const (
	pathPmd    = "/pmd/bin/run.sh"
	pathOutput = "/tmp/pmd-output.xml"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Dir = path
		cmd.Env = os.Environ()
		return cmd
	}

	args := []string{
		"pmd",
		"-no-cache",
		"--failOnViolation", "false",
		"-dir", ".",
		"-format", "xml",
		"-rulesets", "category/apex/security.xml",
		"-reportfile", pathOutput,
	}

	cmd := setupCmd(exec.Command(pathPmd, args...))
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	if err != nil {
		return nil, err
	}

	return os.Open(pathOutput)
}
